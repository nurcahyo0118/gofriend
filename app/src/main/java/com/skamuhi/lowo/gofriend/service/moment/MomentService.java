package com.skamuhi.lowo.gofriend.service.moment;

import com.skamuhi.lowo.gofriend.entity.Page;
import com.skamuhi.lowo.gofriend.entity.moment.Moment;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by error on 27/03/17.
 */

public interface MomentService {

    @GET("api/moments")
    Call<Page<Moment>> callMoment(@Query("access_token") String token);

    @GET("api/moments")
    Call<Page<Moment>> callMoment(@Query("access_token") String token, @Query("q") String keyword);

    @POST("api/moments")
    Call<Moment> postMoment(@Body Moment moment);

}
