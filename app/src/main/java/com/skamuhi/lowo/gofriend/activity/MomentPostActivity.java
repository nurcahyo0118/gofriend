package com.skamuhi.lowo.gofriend.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.path.android.jobqueue.JobManager;
import com.skamuhi.lowo.gofriend.R;
import com.skamuhi.lowo.gofriend.core.commons.User;
import com.skamuhi.lowo.gofriend.entity.moment.Moment;
import com.skamuhi.lowo.gofriend.service.ClientService;
import com.skamuhi.lowo.gofriend.service.moment.MomentService;
import com.skamuhi.lowo.gofriend.utils.AuthenticationUtils;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.firebase.client.DataSnapshot;
//import com.firebase.client.Firebase;
//import com.firebase.client.FirebaseError;
//import com.firebase.client.ValueEventListener;

/**
 * Created by error on 17/03/17.
 */

public class MomentPostActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_caption)
    EditText editTextCaption;
    @BindView(R.id.moment_image_preview)
    ImageButton momentImagePreview;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.btn_add_image)
    RelativeLayout btnAddImage;

    private static final int GALLERY_REQUESRT = 1;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.toolbar_post_moment)
    Toolbar toolbarPostMoment;

    private Uri imageUri = null;

    private ProgressDialog progressDialog;
//
    private Moment moment = null;

    private MomentService momentService;

    private JobManager jobManager;
    private String processId;
    private AuthenticationUtils authenticationUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_post_moment);
        ButterKnife.bind(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        momentService = ClientService.createService().create(MomentService.class);

        setSupportActionBar(toolbarPostMoment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarPostMoment.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUESRT);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUESRT && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            momentImagePreview.setImageURI(imageUri);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.moment_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_send) {
            try {
                postMoment();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (item.getItemId() == R.id.home) {
//            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void postMoment() throws IOException {


        moment = new Moment();
        moment.setCaption("111111111111111111111111111111111");
        moment.setLocation("1111111111111");
        moment.setImage("111111111111111");
        moment.setTag("111111111111111");
        moment.setAuthor(AuthenticationUtils.getCurrentAuthentication().getUser());

        Call<Moment> callPost = momentService.postMoment(moment);
        Response<Moment> response = callPost.execute();

        if (response.isSuccessful()){
            Log.d("post:","MOMENT POSTED!");
        } else {
            Log.d("post:","MOMENT POSTED FAILED!");
        }

//        callPost.enqueue(new Callback<Moment>() {
//            @Override
//            public void onResponse(Call<Moment> call, Response<Moment> response) {
//                Moment moment = response.body();
//            }
//
//            @Override
//            public void onFailure(Call<Moment> call, Throwable t) {
//
//            }
//        });

//        Moment moment = response.body();




    }
}
