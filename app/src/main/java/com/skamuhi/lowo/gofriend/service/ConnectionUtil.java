package com.skamuhi.lowo.gofriend.service;


import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by cahyo on 5/21/16.
 */
public class ConnectionUtil {


    private static final int TIMEOUT = 15000;

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null)
            return cm.getActiveNetworkInfo().isConnectedOrConnecting();
        else
            return false;
    }

    public static HttpParams GetHttpParams(int connectionTimeout, int socketTimeout) {
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, connectionTimeout);
        HttpConnectionParams.setSoTimeout(params, socketTimeout);

//this
        return params;
    }

    public static String convertEntityToString(HttpEntity entity) {
        InputStream instream;
        StringBuilder total = null;
        try {
            instream = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    instream));
            total = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                total.append(line);}
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return total.toString();
    }

    public static JSONObject get(String url) {
        JSONObject json = null;

        try {
            Log.d("ConnectionUtil", "URL Get: " + url);

//   String encodeUrl = URLEncoder.encode(url, "UTF-8");

            HttpClient httpClient = new DefaultHttpClient(GetHttpParams(TIMEOUT, TIMEOUT));
//            HttpClient httpClient = new DefaultHttpClient(GetHttpParams(TIMEOUT, TIMEOUT));
            HttpGet httpGet = new HttpGet(url);

//   Log.d("ConnectionUtil", "URL Get: " + encodeUrl);

            httpGet.setHeader("Content-Type", "application/json");
            HttpResponse response = httpClient.execute(httpGet);

            json = new JSONObject(convertEntityToString(response.getEntity()));
        } catch (IOException e) {
            json = null;
            e.printStackTrace();
        } catch (JSONException e) {
            json = null;
            e.printStackTrace();
        } catch (Exception e) {
            json = null;
            e.printStackTrace();
        }
        return json;
    }

}
