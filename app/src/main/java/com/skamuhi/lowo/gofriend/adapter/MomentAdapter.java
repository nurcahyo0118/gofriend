package com.skamuhi.lowo.gofriend.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.skamuhi.lowo.gofriend.R;
import com.skamuhi.lowo.gofriend.entity.moment.Moment;
import com.skamuhi.lowo.gofriend.service.RestVariables;
import com.skamuhi.lowo.gofriend.utils.AuthenticationUtils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by error on 23/03/17.
 */

public class MomentAdapter extends RecyclerView.Adapter<MomentAdapter.ViewHolder> {

    private List<Moment> momentList = new ArrayList<Moment>();
    private Context context;
    private LayoutInflater layoutInflater;

    public MomentAdapter(Context context,List<Moment> momentList) {
        this.context = context;
        this.momentList = momentList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public MomentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_moment, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MomentAdapter.ViewHolder holder, int position) {
        final Moment moment = momentList.get(position);
        holder.caption.setText(moment.getCaption());
        holder.author.setText(moment.getAuthor().getName().getFirst() + " " + moment.getAuthor().getName().getLast());
        holder.postDate.setText("12 November 2017");
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private FontAppCompatTextView caption;
        private FontAppCompatTextView author, postDate;
        private CircleImageView imageAuthor;
        private Typeface regular, bold, semiBold, extraBold;
        private AssetManager am;

        public ViewHolder(View itemView) {
            super(itemView);

            caption = (FontAppCompatTextView) itemView.findViewById(R.id.txt_caption);
            author = (FontAppCompatTextView) itemView.findViewById(R.id.txt_author);
            postDate = (FontAppCompatTextView) itemView.findViewById(R.id.txt_post_date);
            imageAuthor = (CircleImageView) itemView.findViewById(R.id.image_author);

            String url = RestVariables
                    .SERVER_URL + "api/users/" + AuthenticationUtils
                    .getCurrentAuthentication()
                    .getUser()
                    .getUsername() + "/photo" + "?access_token=" + AuthenticationUtils
                    .getCurrentAuthentication()
                    .getAccessToken();

            Glide.with(context)
                    .load(url)
                    .into(imageAuthor);


        }
    }

    public void addMoment(Moment moment){
        momentList.add(moment);
        notifyDataSetChanged();
    }

    public void addMoment(List<Moment> moments){
        this.momentList.addAll(moments);
        notifyDataSetChanged();
    }

    public void clear(){
        momentList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return momentList.size();
    }

    @Override
    public long getItemId(int id) {
        return 0;
    }
}
