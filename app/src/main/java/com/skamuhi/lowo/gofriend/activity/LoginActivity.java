package com.skamuhi.lowo.gofriend.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.path.android.jobqueue.JobManager;
import com.skamuhi.lowo.gofriend.R;
import com.skamuhi.lowo.gofriend.entity.RestAppication;
import com.skamuhi.lowo.gofriend.event.LoginEvent;
import com.skamuhi.lowo.gofriend.job.LoginManualJob;
import com.skamuhi.lowo.gofriend.utils.AuthenticationUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by error on 08/03/17.
 */

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.edit_text_username)
    EditText editTextUsername;
    @BindView(R.id.txt_register)
    TextView txtRegister;

    private JobManager jobManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        EventBus.getDefault().register(this);

        jobManager = RestAppication.getInstance().getJobManager();


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("Meeeeee", "akshfjshihighsjehfgihsifhgshdfg");
                final ProgressDialog progressDialog = ProgressDialog.show(LoginActivity.this, "", "pleasen wait", false, false);
                LoginManualJob loginJob = new LoginManualJob(editTextUsername.getText().toString(), editTextPassword.getText().toString());
                jobManager.addJobInBackground(loginJob);

            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

    public void onEventMainThread(LoginEvent.LoginSuccess loginSuccess) {
        goToMainActivity();
    }

    public void onEventMainThread(LoginEvent.LoginFailed loginFailed) {
        editTextUsername.setEnabled(true);
        editTextPassword.setEnabled(true);
    }

    private void goToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        EventBus.getDefault().unregister(this);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AuthenticationUtils.getCurrentAuthentication() != null) {
            goToMainActivity();
        }
    }

}
