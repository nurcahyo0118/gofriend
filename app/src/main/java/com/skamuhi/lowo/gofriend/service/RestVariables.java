package com.skamuhi.lowo.gofriend.service;

/**
 * Created by cahyo on 5/21/16.
 */
public class RestVariables {

    public static final String SERVER_URL = "http://10.148.16.134:8080/";
    public static final String SERVER_URL_OAUTH = "http://10.148.16.134:8080";

//    public static final String SERVER_URL = "http://192.168.43.22:8080/";
//    public static final String SERVER_URL_OAUTH = "http://192.168.43.22:8080";

    public static final String PGA_APP_ID = "419c6697-14b7-4853-880e-b68e3731e316";
    public static final String PGA_API_SECRET = "s3cr3t";

//    public static final String PGA_APP_ID = "e2ba0481-b568-46b8-b3c1-5d096a84c439";
//    public static final String PGA_API_SECRET = "dzMvRtFdt9YpdEeJC7NoIX0ZUfllt15t7Hy7kJkOIi70EN9XIIoGm411Ymj395zc";

    public static final int CHALLENGE_GET_TASK = 1;
    public static final int CHALLENGE_POST_TASK = 2;
    public static final int CHALLENGE_DELETE_TASK = 3;
    public static final int CHALLENGE_PUT_TASK = 4;
    public static final int CHALLENGE_GET_ID_TASK = 5;

    public static final String PGA_REQUEST_TOKEN = "/oauth/token";
    public static final String PGA_CURRENT_ME = "/api/users/me";
    public static final String PGA_CURRENT_SITE = "/api/sites/me";
    public static final String PGA_CURRENT_ROLE = "/api/users/me/roles";
}
