package com.skamuhi.lowo.gofriend.service;

/**
 * Created by cahyo on 5/21/16.
 */
public interface TaskService<R> {

    void onExecute(int code);

    void onSuccess(int code, R result);

    void onCancel(int code, String message);

    void onError(int code, String message);
}
