package com.skamuhi.lowo.gofriend.service.user;


import com.skamuhi.lowo.gofriend.core.commons.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by error on 07/06/16.
 */
public interface UserService {

    @GET("api/users/{username}")
    Call<User> callUser(@Path("username") String username, @Query("access_token") String token);

}
