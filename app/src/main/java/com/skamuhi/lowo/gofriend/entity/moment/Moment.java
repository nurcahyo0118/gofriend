package com.skamuhi.lowo.gofriend.entity.moment;

import android.os.Parcel;
import android.os.Parcelable;

import com.skamuhi.lowo.gofriend.core.DefaultPersistence;
import com.skamuhi.lowo.gofriend.core.commons.User;

/**
 * Created by error on 27/03/17.
 */

public class Moment extends DefaultPersistence {

    private String caption;
    private String location;
    private String image;
    private String tag;
    private User author;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
