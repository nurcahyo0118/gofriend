package com.skamuhi.lowo.gofriend.service;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by error on 06/06/16.
 */
public class ClientService {
    public static Retrofit createService(){
        return new Retrofit.Builder()
                .baseUrl(RestVariables.SERVER_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

}
