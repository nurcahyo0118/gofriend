package com.skamuhi.lowo.gofriend.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.skamuhi.lowo.gofriend.R;
import com.skamuhi.lowo.gofriend.adapter.MomentAdapter;
import com.skamuhi.lowo.gofriend.entity.Authentication;
import com.skamuhi.lowo.gofriend.entity.Page;
import com.skamuhi.lowo.gofriend.entity.moment.Moment;
import com.skamuhi.lowo.gofriend.service.ClientService;
import com.skamuhi.lowo.gofriend.service.moment.MomentService;
import com.skamuhi.lowo.gofriend.utils.AuthenticationUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.list_moment)
    RecyclerView listMoment;

    private MomentAdapter momentAdapter;
    private MomentService momentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        momentService = ClientService.createService().create(MomentService.class);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("Go Friend");
        setSupportActionBar(toolbar);

        momentAdapter = new MomentAdapter(this, new ArrayList<Moment>());

        listMoment.setAdapter(momentAdapter);
        listMoment.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listMoment.setLayoutManager(layoutManager);
        listMoment.setItemAnimator(new DefaultItemAnimator());

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, MomentPostActivity.class));

            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        getMoments();
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getMoments(){
        Log.d("token token", AuthenticationUtils.getCurrentAuthentication().getAccessToken());
        Call<Page<Moment>> moment = momentService
                .callMoment(AuthenticationUtils
                        .getCurrentAuthentication()
                        .getAccessToken());
        moment.enqueue(new Callback<Page<Moment>>() {
            @Override
            public void onResponse(Call<Page<Moment>> call, Response<Page<Moment>> response) {
                Page<Moment> momentPage = response.body();
                momentAdapter.clear();
                momentAdapter.addMoment(momentPage.getContent());

                Log.d("content challenge", Integer.toString(momentPage.getContent().size()));
            }

            @Override
            public void onFailure(Call<Page<Moment>> call, Throwable t) {

                Log.d("failed get challenge", t.getMessage());

            }
        });
    }
}
