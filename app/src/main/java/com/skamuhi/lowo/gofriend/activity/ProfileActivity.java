package com.skamuhi.lowo.gofriend.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.skamuhi.lowo.gofriend.R;
import com.skamuhi.lowo.gofriend.service.RestVariables;
import com.skamuhi.lowo.gofriend.utils.AuthenticationUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by error on 27/03/17.
 */

public class ProfileActivity extends AppCompatActivity {
    @BindView(R.id.profile_picture)
    ImageView profilePicture;
    @BindView(R.id.toolbar_profile)
    Toolbar toolbarProfile;
    @BindView(R.id.profile_collapsing)
    CollapsingToolbarLayout profileCollapsing;
    @BindView(R.id.profile_appbar)
    AppBarLayout profileAppbar;
    @BindView(R.id.title_phone)
    FontAppCompatTextView titlePhone;
    @BindView(R.id.txt_title_phone)
    FontAppCompatTextView txtTitlePhone;
    @BindView(R.id.txt_username)
    FontAppCompatTextView txtUsername;


//    private UserService userService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        String url = RestVariables
                .SERVER_URL + "api/users/" + AuthenticationUtils
                .getCurrentAuthentication()
                .getUser()
                .getUsername() + "/photo" + "?access_token=" + AuthenticationUtils
                .getCurrentAuthentication()
                .getAccessToken();

        Glide.with(this)
                .load(url)
                .into(profilePicture);

        setSupportActionBar(toolbarProfile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarProfile.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        profileCollapsing.setTitle(AuthenticationUtils
                .getCurrentAuthentication()
                .getUser()
                .getName()
                .getFirst()
                + " " + AuthenticationUtils
                .getCurrentAuthentication()
                .getUser()
                .getName()
                .getLast());


    }
}
